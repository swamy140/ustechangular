import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/common.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-team-matches',
  templateUrl: './team-matches.component.html',
  styleUrls: ['./team-matches.component.css']
})
export class TeamMatchesComponent implements OnInit {

  constructor(private commonService: CommonService, private router: Router,private route: ActivatedRoute) { }
slug:String;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.slug = (params.get('slug'));
     
    });
    if(this.slug!=null){
      this.getTeamBetweenMatches(this.slug)
    }else{
      this.getMatches();
    }
  }
  isLoader = false;
  matches=[];

  getMatches(){
    this.isLoader = true;
    this.commonService.getMatches().subscribe((data: any) => {
      this.isLoader = false;


      this.matches = data;

      // this.actuals$ = data;
      console.log(data);
      // this.resetForm(form);


    },
      (err: HttpErrorResponse) => {
        
        this.isLoader = false;
        //this.toastr.error('Book Posting Failed..');

      });
  }
  getTeamBetweenMatches(slug){
    this.isLoader = true;
    this.commonService.getTeamBetweenMatches(slug).subscribe((data: any) => {
      this.isLoader = false;


      this.matches = data;

      // this.actuals$ = data;
      console.log(data);
      // this.resetForm(form);


    },
      (err: HttpErrorResponse) => {
        
        this.isLoader = false;
        //this.toastr.error('Book Posting Failed..');

      });
  }

}
