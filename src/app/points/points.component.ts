import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-points',
  templateUrl: './points.component.html',
  styleUrls: ['./points.component.css']
})
export class PointsComponent implements OnInit {

  constructor(private commonService: CommonService, private router: Router,private route: ActivatedRoute) { }
slug;String;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.slug = (params.get('slug'));
    });
    if(this.slug!=null){
      this.getPointsBetweenTeamsBySlug(this.slug);

    }else{
      this.getPointsBetweenTeams();

    }
   

  }
  isLoader = false;
  points:[]

  getPointsBetweenTeamsBySlug(slugVal:any){
    this.isLoader = true;
    this.commonService.getPointsByteamSlug(slugVal).subscribe((data: any) => {
      this.isLoader = false;


      this.points = data;

      // this.actuals$ = data;
      console.log(data);
      // this.resetForm(form);


    },
      (err: HttpErrorResponse) => {
        
        this.isLoader = false;
        //this.toastr.error('Book Posting Failed..');

      });
  }

  getPointsBetweenTeams() {
   
    this.isLoader = true;
    this.commonService.getPoints().subscribe((data: any) => {
      this.isLoader = false;


      this.points = data;

      // this.actuals$ = data;
      console.log(data);
      // this.resetForm(form);


    },
      (err: HttpErrorResponse) => {
        
        this.isLoader = false;
        //this.toastr.error('Book Posting Failed..');

      });
  }

}
