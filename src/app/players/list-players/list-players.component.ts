import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/common.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-list-players',
  templateUrl: './list-players.component.html',
  styleUrls: ['./list-players.component.css']
})
export class ListPlayersComponent implements OnInit {

  constructor(private router: Router,private route: ActivatedRoute,private commonService:CommonService) { }
slug:String;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.slug = (params.get('slug'));
      
    });
    if(this.slug!=null){
      this.getPlayerDetailsBySlug(this.slug);
    }else{
      this.getPlayerDetailsBy();
    }
  }

  isLoader = false;
  players = [];
  getPlayerDetailsBy(){
    this.isLoader = true;
    this.commonService.getListOfPlayers().subscribe((data: any) => {
      this.isLoader = false;


      this.players = data;

      // this.actuals$ = data;
      console.log(data);
      // this.resetForm(form);


    },
      (err: HttpErrorResponse) => {
        this.isLoader = false;
        //this.toastr.error('Book Posting Failed..');

      });
  }
  
  getPlayerDetailsBySlug(slug){
    this.isLoader = true;
    this.commonService.getPlyaerByTeam(slug).subscribe((data: any) => {
      this.isLoader = false;


      this.players = data;

      // this.actuals$ = data;
      console.log(data);
      // this.resetForm(form);


    },
      (err: HttpErrorResponse) => {
        this.isLoader = false;
        //this.toastr.error('Book Posting Failed..');

      });
  }
 

}
