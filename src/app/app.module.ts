import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PlayersComponent } from './players/players.component';
import { TeamsComponent } from './teams/teams.component';
import { CreatePlayerComponent } from './players/create-player/create-player.component';
import { UpdatePlayerComponent } from './players/update-player/update-player.component';
import { UpdateTeamComponent } from './teams/update-team/update-team.component';
import { CreateTeamComponent } from './teams/create-team/create-team.component';
import { ListTeamsComponent } from './teams/list-teams/list-teams.component';
import { ListPlayersComponent } from './players/list-players/list-players.component';
import { HttpClientModule } from '@angular/common/http';
import { PointsComponent } from './points/points.component';
import { TeamMatchesComponent } from './teams/team-matches/team-matches.component';
import { FormsModule,FormBuilder } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlayersComponent,
    TeamsComponent,
    CreatePlayerComponent,
    UpdatePlayerComponent,
    UpdateTeamComponent,
    CreateTeamComponent,
    ListTeamsComponent,
    ListPlayersComponent,
    PointsComponent,
    TeamMatchesComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [FormBuilder],
  bootstrap: [AppComponent]
})
export class AppModule { }
