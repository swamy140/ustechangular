import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TeamsComponent } from './teams/teams.component';
import { combineLatest } from 'rxjs';
import { PlayersComponent } from './players/players.component';
import { createComponent } from '@angular/compiler/src/core';
import { CreateTeamComponent } from './teams/create-team/create-team.component';
import { UpdateTeamComponent } from './teams/update-team/update-team.component';
import { CreatePlayerComponent } from './players/create-player/create-player.component';
import { UpdatePlayerComponent } from './players/update-player/update-player.component';
import { ListTeamsComponent } from './teams/list-teams/list-teams.component';
import { ListPlayersComponent } from './players/list-players/list-players.component';
import { PointsComponent } from './points/points.component';
import { TeamMatchesComponent } from './teams/team-matches/team-matches.component';


const routes: Routes = [
  {
    path: '', component: ListTeamsComponent,
  },
  {
    path: 'teams', component: TeamsComponent,
    children: [
      {
        path: "", component: ListTeamsComponent
      },

      {
        path: "create", component: CreateTeamComponent
      }, {
        path: "update", component: UpdateTeamComponent
      }
    ]
  },
  {
    path: "teams/:slug", component: TeamMatchesComponent
  },{
    path:"matches",component:TeamMatchesComponent
  },
  {
    path: "points", component: PointsComponent

  },
  {
    path: "points/:slug", component: PointsComponent
  },
  {
    path: "player", component: PlayersComponent,
    children: [
      {
        path: "list", component: ListPlayersComponent
      },
      {
        path: ":slug", component: ListPlayersComponent
      },
      {
        path: "create", component: CreatePlayerComponent
      },
      {
        path: "update", component: UpdatePlayerComponent
      }
    ]

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
